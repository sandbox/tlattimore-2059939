<?php

// Declare our namespace.
// See: http://php.net/manual/en/language.namespaces.rationale.php.
namespace Drupal\hello_eight\Controller;

// Import Drupal core and Symfony 2 stuff.
// See: http://php.net/manual/en/language.namespaces.importing.php.
use Drupal\Core\Controller\ControllerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

// Create a class that implements ControllerInterface.
class HelloEightController implements ControllerInterface {

  // Call the "create()" method for ControllerInterface.
	public static function create(ContainerInterface $container) {
		return new static($container->get('module_handler'));
	}

	public function HelloEightPage() {
		$build = array(
			'#type' => 'markup',
			'#markup' => t('Hello, Drupal 8!'),
		);
		return  $build;
	}
}
